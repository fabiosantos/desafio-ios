//
//  PullRequestTableViewCell.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 12/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var creationDateLabel: UILabel!
    
    @IBOutlet weak var userLogin: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
