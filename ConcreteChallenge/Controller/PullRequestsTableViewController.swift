//
//  PullRequestsTableViewController.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestsTableViewController: UITableViewController, SFSafariViewControllerDelegate {

    // MARK: - Properties
    
    var detailItem: Repository? {
        didSet{
            self.refreshView()
        }
    }
    
    var pullRequests = [PullRequest]()
    var nextPageURLString: String?
    var isLoading = false
    
    
    // MARK: Types
    private struct TableViewConstants {
        static let tableViewCellIdentifier = "pullRequestCell"
    }
    
    
    // MARK: - Helper methods
    
    private func refreshView() {
        loadPullRequests(urlToLoad: nil)
    }
    
    private func loadPullRequests(urlToLoad: String?) {
        
        guard let repo = detailItem else {
            print("No repo")
            return
        }
        
        GithubAPIManager.shared.fetchPullRequests(pageToLoad: urlToLoad,forRepo:repo) {
            (result, nextPage) in
            
            self.isLoading = false
            self.nextPageURLString = nextPage
            
            guard result.error == nil else {
                // Handle errors.
                return
            }
            
            guard let fetchedPRs = result.value else {
                print("no pr list!")
                return
            }
            
            if urlToLoad == nil {
                self.pullRequests = []
            }

            self.pullRequests += fetchedPRs
            
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.tableView.rowHeight = 90.0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = detailItem?.name
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.pullRequests.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewConstants.tableViewCellIdentifier, for: indexPath) as? PullRequestTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PullRequestTableViewCell.")
        }
        
        let pullRequest = self.pullRequests[indexPath.row]
        
        cell.titleLabel?.text = pullRequest.title
        cell.bodyLabel.text = pullRequest.body
        cell.userLogin.text = pullRequest.userLogin
        cell.creationDateLabel.text = Timestamps.dateString(pullRequest.createdAt)
        cell.userImageView?.image = nil
        
        // Loading images
        if let urlString = pullRequest.avatarURL {
            GithubAPIManager.shared.imageFrom(urlString: urlString) {
                (image, error) in
                guard error == nil else {
                    print(error!)
                    return
                }

                if let cellToUpdate = self.tableView?.cellForRow(at: indexPath) as? PullRequestTableViewCell {
                    cellToUpdate.userImageView?.image = image
                    cellToUpdate.setNeedsLayout()
                }
            }
        }
        
        // Do we need to load more prs?
        if !isLoading {
            let rowsLoaded = pullRequests.count
            let rowsRemaining = rowsLoaded - indexPath.row
            let rowsToLoadFromBottom = 5
            if rowsRemaining <= rowsToLoadFromBottom {
                if let nextPage = nextPageURLString {
                    self.loadPullRequests(urlToLoad: nextPage)
                }
            }
        }
        return cell
    }
    
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let urlString = pullRequests[indexPath.row].htmlURL
        
        if let url = urlString {
            let vc = SFSafariViewController(url: url)
            vc.delegate = self
            
            present(vc, animated: true)
        }
    }
    
    
    // MARK: - SafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        dismiss(animated: true)
    }
}
