//
//  ReposTableTableViewController.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//
import UIKit

class ReposTableViewController: UITableViewController, SegueHandlerType {
    
    // MARK: - Properties
    
    var repos = [Repository]()
    var nextPageURLString: String?
    var isLoading = false
    
    
    // MARK: Types
    
    struct TableViewConstants {
        static let tableViewCellIdentifier = "repoCell"
    }
    
    enum SegueIdentifier: String {
        case ShowPullRequests = "ShowPullRequests"
    }
    
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 90.0
        loadRepos(urlToLoad: nil)
    }
    
    
    // MARK: - Helper methods
    
    private func loadRepos(urlToLoad: String?) {
        GithubAPIManager.shared.fetchPublicRepos(pageToLoad: urlToLoad) {
            (result, nextPage) in
            
            self.isLoading = false
            self.nextPageURLString = nextPage
            
            guard result.error == nil else {
                // Handle errors.
                return
            }
            
            guard let fetchedReposList = result.value else {
                print("no repos list!")
                return
            }
            
            if urlToLoad == nil {
                // empty out the repos because we're not loading another page
                self.repos = []
            }
            
            guard let fetchedRepos = fetchedReposList.items else {
                print("no repos!")
                return
            }
            
            self.repos += fetchedRepos
            
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return repos.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewConstants.tableViewCellIdentifier, for: indexPath) as? RepositoryTableViewCell  else {
            fatalError("The dequeued cell is not an instance of RepositoryTableViewCell.")
        }
        
        let repo = repos[indexPath.row]
        
        cell.nameLable?.text = repo.name
        cell.descriptionLabel.text = repo.description
        cell.authorLoginLabel.text = repo.ownerLogin
        cell.authorImageView.image = nil
        cell.forksLabel.text = "⑂ \(repo.forks)"
        cell.starsLabel.text = "★ \(repo.stars)"
        
        
        // Loading images
        if let urlString = repo.ownerImageURL {
            GithubAPIManager.shared.imageFrom(urlString: urlString) {
                (image, error) in
                guard error == nil else {
                    print(error!)
                    return
                }
                
                if let cellToUpdate = self.tableView?.cellForRow(at: indexPath) as? RepositoryTableViewCell {
                    cellToUpdate.authorImageView?.image = image
                    cellToUpdate.setNeedsLayout()
                }
            }
        }
        
        // Do we need to load more repos?
        if !isLoading {
            let rowsLoaded = repos.count
            let rowsRemaining = rowsLoaded - indexPath.row
            let rowsToLoadFromBottom = 5
            if rowsRemaining <= rowsToLoadFromBottom {
                if let nextPage = nextPageURLString {
                    self.loadRepos(urlToLoad: nextPage)
                }
            }
        }
        return cell
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        switch segueIdentifierForSegue(segue: segue) {
        case .ShowPullRequests:
            
            if let selectedRepoIndexPath = self.tableView.indexPathForSelectedRow {
                let repo = repos[selectedRepoIndexPath.row]
                
                let repoDetailViewController = segue.destination as? PullRequestsTableViewController
                
                if let repoDetailVC = repoDetailViewController {
                    
                    repoDetailVC.detailItem = repo
                }
            }
        }
    }
}

