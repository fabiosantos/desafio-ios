//
//  PullRequest+Network.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 11/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation
import Alamofire

extension PullRequest {
    
    convenience init?(json: [String: Any]) {
        
        guard let urlString = json["html_url"] as? String, let url = URL(string: urlString) else {
            return nil
        }
        
        guard let title = json["title"] as? String else {
            return nil
        }
        
        guard let body = json["body"] as? String else {
            return nil
        }
        
        let createdAt = Timestamps.iso8601Date(json["created_at"] as? String)
        
        guard let user = json["user"] as? [String: Any] else {
            return nil
        }
        
        guard let login = user["login"] as? String else {
            return nil
        }
        
        guard let avatarURL = user["avatar_url"] as? String else {
            return nil
        }
        
        self.init(htmlURL: url,title: title, body: body, createdAt: createdAt ,userLogin: login, avatarURL: avatarURL)
    }
}
