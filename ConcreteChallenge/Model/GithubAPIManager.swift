//
//  GithubAPIManager.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//
import Foundation
import Alamofire

enum GitHubAPIManagerError: Error {
    case network(error: Error)
    case apiProvidedError(reason: String)
    case objectSerialization(reason: String)
}

class GithubAPIManager {
    
    static let shared = GithubAPIManager()
    
    
    // MARK: - Pagination
    
    private func parseNextPageFromHeaders(response: HTTPURLResponse?) -> String? {
        
        guard let linkHeader = response?.allHeaderFields["Link"] as? String else {
            return nil
        }
     
        let components = linkHeader.split(separator: ",").map { String($0) }
        
        for item in components {
            
            let rangeOfNext = item.range(of: "rel=\"next\"", options: [])
            guard rangeOfNext != nil else {
                continue
            }

            let rangeOfPaddedURL = item.range(of: "<(.*)>;",
                                              options: .regularExpression,
                                              range: nil,
                                              locale: nil)
            guard let range = rangeOfPaddedURL else {
                return nil
            }
            let nextURL = item[range]

            let start = nextURL.index(range.lowerBound, offsetBy: 1)
            let end = nextURL.index(range.upperBound, offsetBy: -2)
            let trimmedRange = start ..< end
            let string = String(nextURL[trimmedRange])
            return string
        }
        return nil
    }
    
    
    // MARK: - Pull Request
    
    private func pullRequestArrayFromReponse(response: DataResponse<Any>) -> Result<[PullRequest]> {
        
        guard response.result.error == nil else {
            print(response.result.error!)
            return .failure(GitHubAPIManagerError.network(error:response.result.error!))
        }

        if let jsonDictionary = response.result.value as? [String: Any],
            let errorMessage = jsonDictionary["message"] as? String {
            return .failure(GitHubAPIManagerError.apiProvidedError(reason: errorMessage))
        }

        guard let jsonArray = response.result.value as? [[String: Any]] else {

            print("Did not get JSON dict from API")
            return .failure(GitHubAPIManagerError.objectSerialization(reason: "Did not get JSON dict in response"))
        }
        
        // Mapping
        var pullRequests = [PullRequest]()

        for item in jsonArray {
            if let pullRequest = PullRequest(json: item) {
                pullRequests.append(pullRequest)
            }
        }
        return .success(pullRequests)
    }
    
    func fetchPullRequests(pageToLoad: String?,forRepo: Repository, completionHandler:
        @escaping (Result<[PullRequest]>, String?) -> Void) {
        if let urlString = pageToLoad {
            print("Pagination: loading more PRs...")
            fetchPullRequests(GitHubRouter.getAtPath(urlString), completionHandler: completionHandler)
        } else {
            fetchPullRequests(GitHubRouter.getPullRequestsForUser(forRepo.ownerLogin, forRepo.name), completionHandler: completionHandler)
        }
    }
    
    func fetchPullRequests(_ urlRequest: URLRequestConvertible, completionHandler: @escaping (Result<[PullRequest]>, String?) -> Void) {
        
        Alamofire.request(urlRequest).validate().responseJSON { response in
            
            let result = self.pullRequestArrayFromReponse(response: response)
            let next = self.parseNextPageFromHeaders(response: response.response)
            completionHandler(result,next)
        }
    }
    
    // MARK: - Repository
    
    // Parse the response to get a repository list.
    private func repositoryListFromReponse(response: DataResponse<Any>) -> Result<RepositoryList> {
        
        guard response.result.error == nil else {
            
            print(response.result.error!)
            return .failure(GitHubAPIManagerError.network(error:response.result.error!))
        }
        
        guard let jsonDictionary = response.result.value as? [String: Any] else {
            
            print("Did not get JSON dict from API")
            return .failure(GitHubAPIManagerError.objectSerialization(reason: "Did not get JSON dict in response"))
        }
        
        guard let repoList = RepositoryList(json: jsonDictionary) else {
            print("Error: JSON Mapping")
            return .failure(GitHubAPIManagerError.objectSerialization(reason: "Could not map JSON dict in response"))
        }
        
        return .success(repoList)
    }
    
    func fetchPublicRepos(pageToLoad: String?, completionHandler:
        @escaping (Result<RepositoryList>, String?) -> Void) {
        if let urlString = pageToLoad {
            fetchRepos(GitHubRouter.getAtPath(urlString), completionHandler: completionHandler)
        } else {
            fetchRepos(GitHubRouter.getJavaReposByStarsWithPage(1), completionHandler: completionHandler)
        }
    }
    
    func fetchRepos(_ urlRequest: URLRequestConvertible,completionHandler: @escaping (Result<RepositoryList>, String?) -> Void) {

        Alamofire.request(urlRequest)
            .responseJSON { response in
                
                let result = self.repositoryListFromReponse(response: response)
                let next = self.parseNextPageFromHeaders(response: response.response)
                completionHandler(result,next)
        }
    }
    
    
    // MARK: - Image
    
    func imageFrom(urlString: String, completionHandler: @escaping (UIImage?, Error?) -> Void) {
        let _ = Alamofire.request(urlString).response { dataResponse in
            
            guard let data = dataResponse.data else {
                completionHandler(nil, dataResponse.error)
                return
            }
            
            let image = UIImage(data: data)
            completionHandler(image, nil)
        }
    }
}

