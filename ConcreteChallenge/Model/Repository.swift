//
//  Repository.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation

class Repository: Codable {
    var id: Int
    var name: String
    var description: String
    var url: String
    var ownerLogin: String
    var ownerImageURL: String?
    
    var forks: Int
    var stars: Int
    
    init?(id: Int, name: String, description: String,url: String, ownerLogin: String, ownerImageURL: String?, forks: Int, stars: Int) {
        self.id = id
        self.name = name
        self.description = description
        self.url = url
        self.ownerLogin = ownerLogin
        self.ownerImageURL = ownerImageURL
        self.forks = forks
        self.stars = stars
    }
}
