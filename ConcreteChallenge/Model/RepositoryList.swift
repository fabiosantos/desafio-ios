//
//  RepositoryList.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation

class RepositoryList {
    
    var count: Int
    var items: [Repository]?
    var incompleteResults: Bool
    
    required init?(count: Int, incompleteResults: Bool, items: [Repository]?) {
        self.count = count
        self.incompleteResults = incompleteResults
        self.items = items
    }
}

