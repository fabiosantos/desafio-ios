//
//  Repository+Network.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation
import Alamofire

extension Repository {
    
    convenience init?(json: [String: Any]) {
                
        guard let id = json["id"] as? Int else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let description = json["description"] as? String else {
            return nil
        }
        
        guard let forks = json["forks_count"] as? Int else {
            return nil
        }
        
        guard let stars = json["stargazers_count"] as? Int else {
            return nil
        }
        
        guard let url = json["url"] as? String else {
            return nil
        }
        
        
        guard let owner = json["owner"] as? [String: Any],
            let ownerLogin = owner["login"] as? String else {
            return nil
        }
        
        let ownerImageURL = owner["avatar_url"] as? String
        
        self.init(id: id, name: name, description: description, url: url, ownerLogin: ownerLogin, ownerImageURL: ownerImageURL, forks: forks, stars: stars)
    }
}
