//
//  PullRequest.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 11/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation

class PullRequest {
    
    var htmlURL: URL?
    var title: String
    var body: String
    var createdAt: Date?
    var userLogin: String
    var avatarURL: String?
    
    init?(htmlURL: URL?, title: String, body: String, createdAt: Date?,userLogin: String, avatarURL: String?) {
        self.htmlURL = htmlURL
        self.title = title
        self.body = body
        self.createdAt = createdAt
        self.userLogin = userLogin
        self.avatarURL = avatarURL
    }
}
