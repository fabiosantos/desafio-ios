//
//  RepositoryList+Network.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation

extension RepositoryList {
    
    convenience init?(json: [String: Any]) {
        
        guard let count = json["total_count"] as? Int else {
            return nil
        }
        
        guard let incompleteResults = json["incomplete_results"] as? Bool else {
            return nil
        }
        
        guard let reposArray = json["items"] as? [[String: Any]] else {
            return nil
        }
        
        var repos = [Repository]()
        
        for item in reposArray {
            if let repo = Repository(json: item) {
                repos.append(repo)
            }
        }
        
        self.init(count: count, incompleteResults: incompleteResults, items: repos)
    }
}
