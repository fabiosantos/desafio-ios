//
//  GitHubRouter.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 10/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation
import Alamofire

enum GitHubRouter: URLRequestConvertible {
    
    static let baseURLString = "https://api.github.com"
    
    case getPullRequestsForUser(String, String)
    case getPullRequests(String)
    case getJavaReposByStarsWithPage(Int)
    case getAtPath(String)
    
    func asURLRequest() throws -> URLRequest {
        
        var method: HTTPMethod {
            switch self {
            case .getPullRequests, .getPullRequestsForUser,.getJavaReposByStarsWithPage, .getAtPath:
                return .get
            }
        }
        
        let params: ([String: Any]?) = {
            
            switch self {
            case .getPullRequests,.getPullRequestsForUser,.getJavaReposByStarsWithPage, .getAtPath:
                return nil
            }
        }()

        let url: URL = {

            let relativePath: String?
            let query:String?
            
            switch self {
            case .getAtPath(let path):
                return URL(string: path)!
                
            case .getPullRequests(let repoName):
                relativePath = "/repos/\(repoName)/pulls"
                query = nil
            case .getPullRequestsForUser(let userName, let repoName):
                relativePath = "/repos/\(userName)/\(repoName)/pulls"
                query = "state=all&sort=updated&order=desc"
            case .getJavaReposByStarsWithPage(let page):
                relativePath = "/search/repositories"
                query = "q=language:Java&sort=stars&order=desc&page=\(page)"
            }
            var urlComponents = URLComponents(string: GitHubRouter.baseURLString)!
            
            if let relativePath = relativePath {
                urlComponents.path = relativePath
            }
            
            if let query = query {
                urlComponents.query = query
            }
            
            let url = urlComponents.url!
            return url
        }()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        let encoding = JSONEncoding.default
        return try encoding.encode(urlRequest, with: params)
    }
}
