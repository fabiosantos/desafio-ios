//
//  Clock.swift
//  ConcreteChallenge
//
//  Created by Fabio Santos on 12/11/17.
//  Copyright © 2017 Fabio Santos. All rights reserved.
//

import Foundation

struct Timestamps {
    
    // iOS 10 and later
//    fileprivate static var iso8601DateFormatter: ISO8601DateFormatter = {
//        let formatter = ISO8601DateFormatter()
//        return formatter
//    }()
    
    // < iOS 10.0
    fileprivate static var iso8601DateFormatter: DateFormatter = {
        let RFC3339DateFormatter = DateFormatter()
        RFC3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
        RFC3339DateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        RFC3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return RFC3339DateFormatter
    }()
    
    fileprivate static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
    
    static func iso8601Date(_ string: String?) -> Date? {
        guard let string = string else { return nil }
        return Timestamps.iso8601DateFormatter.date(from: string)
    }
    
    static func dateString(_ date: Date?) -> String? {
        guard let date = date else { return nil }
        return Timestamps.dateFormatter.string(from: date)
    }
}
